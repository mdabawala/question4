﻿// using System;
// using System.Collections.Generic;

// namespace InterviewQuestions
// {
//     static class Logger
//     {
//         public static void Write(string message, params string[] parameters)
//         {
//             // Just a stub, not meant to be refactored
//             //Console.Write(message);
//         }
//     }

//     /// <summary>
//     /// I was working very late last night, and my code was not as clean as I would normally like.
//     /// Please help me out, and refactor this to be what you would consider --production-- quality code.
//     /// Please fix anything you would consider a bug, but make sure I understand why it is a bug.
//     /// </summary>
//     public class Question4
//     {
//         private int NumberOfCars { get; set; }
//         private List<Book> Library { get; set; }
//         internal static void Run()
//         {
//             var book = new Book(
//                 new CheckoutStrategyAddToFeeIfMoreThanThresholdDays(2, 3),
//                 new PreferredCustomerFixedDiscountStrategy(1/1.1M))
//             {
//                 DaysRented = 2,
//                 Price = 1.99M,
//                 Title = "Book 1"
//             };
//             var car = new Car(new CheckoutStrategyMultiplyToFeeIfLessThanThresholdDays(1.4M, 3),
//                 new PreferredCustomerFixedDiscountStrategy(0.1M))
//             {
//                 CarType = "Honda",
//                 Desc = "CRX",
//                 DaysRented = 1,
//                 Price = 165M
//             };
//             var movie = new Movie(
//                 new CheckoutStrategyAddToFeeIfMoreThanThresholdDays(10, 3),
//                 new PreferredCustomerFixedDiscountStrategy(1.1M)
//                 )
//             {
//                 MovieName = "Star wars",
//                 DaysRented = 14,
//                 Rating = 5,
//                 Price = 1M
//             };

//             var bobbyRay = new Customer { Name = "Bobby Ray", AgeGroup = Constraints.Child };
//             var bobJohnson = new Customer
//             {
//                 Name = "Bob Johnson",
//                 AgeGroup = Constraints.AdultOnly,
//                 IsPreferredCustomer = true
//             };

//             Store.HandleCustomer(bobbyRay, car);
//             Store.HandleCustomer(bobJohnson, book);
//             Store.HandleCustomer(bobJohnson, movie);
//         }
//     }

//     interface ICheckoutStrategy
//     {
//         decimal CalculateCheckoutFees(decimal fee, int days);
//     }

//     interface IPreferredCustomerStrategy
//     {
//         decimal RecalculateFees(decimal fee);
//     }

//     class PreferredCustomerFixedDiscountStrategy : IPreferredCustomerStrategy
//     {
//         private readonly decimal _multiplyingFactor;
//         public PreferredCustomerFixedDiscountStrategy(decimal multiplyingFactor)
//         {
//             this._multiplyingFactor = multiplyingFactor;
//         }
//         public decimal RecalculateFees(decimal fee)
//         {
//             return fee * this._multiplyingFactor;
//         }
//     }

//     class CheckoutStrategyAddToFeeIfMoreThanThresholdDays : ICheckoutStrategy
//     {
//         private readonly decimal _amountToAdd;
//         private readonly int _thresholdDays;
//         public CheckoutStrategyAddToFeeIfMoreThanThresholdDays(decimal amountToAdd, int thresholdDays)
//         {
//             this._amountToAdd = amountToAdd;
//             this._thresholdDays = thresholdDays;
//         }
//         public decimal CalculateCheckoutFees(decimal fee, int days)
//         {
//             if (days > _thresholdDays)
//                 return fee + this._amountToAdd;
//             else
//                 return fee;
//         }
//     }

//     class CheckoutStrategyAddToFeeIfLessThanThresholdDays : ICheckoutStrategy
//     {
//         private readonly decimal _amountToAdd;
//         private readonly int _thresholdDays;
//         public CheckoutStrategyAddToFeeIfLessThanThresholdDays(decimal amountToAdd, int thresholdDays)
//         {
//             this._amountToAdd = amountToAdd;
//             this._thresholdDays = thresholdDays;
//         }
//         public decimal CalculateCheckoutFees(decimal fee, int days)
//         {
//             if (days < _thresholdDays)
//                 return fee + this._amountToAdd;
//             else
//                 return fee;
//         }
//     }

//     //Movie Strategy
//     class CheckoutStrategyMultiplyToFeeIfLessThanThresholdDays : ICheckoutStrategy
//     {
//         private readonly decimal _multiplyingFactor;
//         private readonly int _thresholdDays;
//         public CheckoutStrategyMultiplyToFeeIfLessThanThresholdDays(decimal multiplyingFactor, int thresholdDays)
//         {
//             this._multiplyingFactor = multiplyingFactor;
//             this._thresholdDays = thresholdDays;
//         }
//         public decimal CalculateCheckoutFees(decimal fee, int days)
//         {
//             if (days < _thresholdDays)
//                 return fee * this._multiplyingFactor;
//             else
//                 return fee;
//         }
//     }

//     class Store
//     {
//         public static void HandleCustomer(Customer customer, BaseItem item)
//         {
//             if (item.CanRent(customer.AgeGroup))
//             {
//                 item.DisplayCost();
//                 Console.WriteLine("Final cost: {0:00.00}", CalculateFees(item, ReturnedCondition.Good, customer));
//             }
//             else
//                 Console.WriteLine($"{customer.Name} cannot rent {item}");
//         }

//         public static decimal CalculateFees(BaseItem item, ReturnedCondition condition, Customer customer)
//         {
//             var fees = 0M;
//             var baseFee = item.Price * item.DaysRented;
//             fees = item.CalculateCheckoutFees(baseFee, item.DaysRented);

//             if (condition == ReturnedCondition.Damaged)
//                 fees += item.DamageItemFee; //Refactor;

//             if (customer.IsPreferredCustomer)
//                 fees = item.RecalculateFees(fees);

//             return fees;
//         }
//     }

//     class Customer
//     {
//         public string Name { get; set; }
//         public bool IsPreferredCustomer { get; set; }
//         public Constraints AgeGroup { get; set; }
//     }

//     enum ReturnedCondition
//     {
//         Good,
//         Damaged,
//         Perfect
//     }

//     [Flags]
//     enum Constraints
//     {
//         Child = 1,
//         Y8 = 2,
//         Y13 = 4,
//         Teen = 8,
//         YoungAdult = 16,
//         //for Flags it has to 32 , 2^5 or 100000 bitwise
//         AdultOnly = 32
//     }

//     abstract class BaseItem
//     {
//         private ICheckoutStrategy Checkout { get; set; }
//         private IPreferredCustomerStrategy PreferredCustomerAdjustment { get; set; }

//         public decimal CalculateCheckoutFees(decimal fees, int days)
//         {
//             return this.Checkout.CalculateCheckoutFees(fees, days);
//         }

//         public decimal RecalculateFees(decimal fee)
//         {
//             return this.PreferredCustomerAdjustment.RecalculateFees(fee);
//         }
//         public BaseItem(ICheckoutStrategy checkout, IPreferredCustomerStrategy preferred)
//         {
//             this.Checkout = checkout;
//             this.PreferredCustomerAdjustment = preferred;
//         }
//         readonly int DEFAULT_DAMAGE_FEE = 500;
//         public abstract Constraints PermittedGroups { get; }
//         public string Desc { get; set; }
//         public decimal Price { get; set; }
//         public virtual decimal DamageItemFee
//         {
//             get
//             {
//                 return DEFAULT_DAMAGE_FEE;
//             }
//         }
//         public int DaysRented { get; set; }
//         public bool CanRent(Constraints c)
//         {
//             return PermittedGroups.HasFlag(c);
//         }
//         public void DisplayCost()
//         {
//             Logger.Write("Entered DisplayCost");
//             try
//             {
//                 Console.WriteLine($"Rented {this.GetType().Name} for {DaysRented} days.");
//                 Console.WriteLine(this.ToString() + " - " + Desc + " " + Price * DaysRented);
//             }
//             catch (Exception ex)
//             {
//                 Logger.Write("DisplayCost: {0}", ex.ToString());
//             }
//             Logger.Write("Exit DisplayCost");
//         }
//     }

//     class Book : BaseItem
//     {
//         public Book(ICheckoutStrategy checkout, IPreferredCustomerStrategy preferred)
//                     : base(checkout, preferred)
//         {

//         }
//         public string Title { get; set; }
//         public override Constraints PermittedGroups { get { return Constraints.AdultOnly | Constraints.YoungAdult | Constraints.Teen | Constraints.Y13; } }
//         public DateTime CheckedOut { get; set; }
//         public override decimal DamageItemFee { get { return 35; } }

//         public override string ToString()
//         {
//             return this.Title;
//         }
//     }
//     class Car : BaseItem
//     {
//         public Car(ICheckoutStrategy checkout, IPreferredCustomerStrategy preferred)
//                     : base(checkout, preferred)
//         {

//         }
//         public string CarType { get; set; }
//         public override Constraints PermittedGroups { get { return Constraints.AdultOnly; } }
//         public DateTime CheckedOut { get; set; }
//         public bool IsDamaged { get; set; }

//         public override string ToString()
//         {
//             return this.CarType;
//         }
//     }
//     class Movie : BaseItem
//     {
//         public Movie(ICheckoutStrategy checkout, IPreferredCustomerStrategy preferred)
//                   : base(checkout, preferred)
//         {

//         }
//         public DateTime CheckedOut { get; set; }
//         public override Constraints PermittedGroups { get { return Constraints.AdultOnly | Constraints.YoungAdult | Constraints.Teen | Constraints.Y13; } }
//         public int Rating { get; set; }
//         public string MovieName;
//         public bool IsDamaged { get; set; }
//         public override decimal DamageItemFee { get { return 20; } }

//         public override string ToString()
//         {
//             return this.MovieName;
//         }
//     }
// }